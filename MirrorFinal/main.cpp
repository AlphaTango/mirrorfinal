/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	MAIN.CPP					(c) 2015 AlphaTangoVideo				WEB: https://alphatangovideo.wordpress.com/
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include						<math.h>
#include						"main.h"
#include						"resource1.h"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Elektronika Mirror effect v1.0
//	todo:	- move alib100 etc sdk libraries in project folder
//			- try and add on-beat mirror type auto change?
//			-- need to figure out what 'dbeat' relates to? presumably downbeat.
//			- Redo UI
//			-- Add 'bypass' button
//			-- Add 'Auto' or 'OnBeat' button?
//			- Optimise xdiag mirror performance (slow @1080p 25fps)
//			-- optimise generally (try and use fewer Abitmaps?)
//			- Documentation
//			- See if 'Minimize' module is possible/plausible? (guess it won't re-arrange the other modules in rack tho)
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ACI								AmirrorInfo::CI	= ACI("AmirrorInfo",	GUID(0x50000000,0x00000320), &AeffectInfo::CI, 0, NULL);
ACI								Amirror::CI		= ACI("Amirror",		GUID(0x50000000,0x00000321), &Aeffect::CI, 0, NULL);
ACI								AmirrorFront::CI	= ACI("AmirrorFront",	GUID(0x50000000,0x00000322), &AeffectFront::CI, 0, NULL);
ACI								AmirrorBack::CI	= ACI("AmirrorBack",	GUID(0x50000000,0x00000323), &AeffectBack::CI, 0, NULL);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int						count=0;
static Aresource				resdll=Aresource("mirror", GetModuleHandle("mirror.dll"));
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


static void init()
{
	if(count==0)
	{
	}
	count++;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void end()
{
	count--;
	if(count==0)
	{
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Amirror::Amirror(QIID qiid, char *name, AeffectInfo *info, Acapsule *capsule) : Aeffect(qiid, name, info, capsule)
{
	init();

	top = NULL;
	left = NULL;

	quart=NULL;
	mirror=NULL;
	bs=null;

	beatCount = 1;

//	Atable *t = capsule->table;
//	t->

	front=new AmirrorFront(qiid, "mirror front", this, 64);
	front->setTooltips("Mirror");
	back=new AmirrorBack(qiid, "mirror back", this, 64);
	back->setTooltips("Mirror");

	settings(false);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Amirror::~Amirror()
{
	if(quart)
		delete(quart);
	if(mirror)
		delete(mirror);
	if(bs)
		delete(bs);
	
	if(top)
		delete(top);
	if(left)
		delete(left);
	end();	// global free - needs to be last/after delete()
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool Amirror::load(class Afile *f)
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool Amirror::save(class Afile *f)
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//bool Amirror::loadPreset(class Afile *f)
//{
//	return load(f);
//}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//bool Amirror::savePreset(class Afile *f)
//{
//	return save(f);
//}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Amirror::settings(bool emergency)
{

	int		w=getVideoWidth();
	int		h=getVideoHeight();

	if(quart)
		quart->size(w>>1, h>>1);
	else
		quart=new Abitmap(w>>1, h>>1);

	if(mirror)
		mirror->size(w, h);
	else
		mirror=new Abitmap(w, h);

	if(bs)
		bs->size(w,h);
	else
		bs = new Abitmap(w,h);

	if(top)
		top->size(w, h>>1);
	else
		top = new Abitmap(w, h>>1);

	if(left)
		left->size(w>>1, h);
	else
		left = new Abitmap(w>>1, h);

	//	table->videoW or table->videoH have been changed
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Amirror::action(double time, double dtime, double beat, double dbeat)
{
	int				w=getVideoWidth();	//is this needed? It's already set in settings(), which is called by the constructor and/or when window size changes...
	int				h=getVideoHeight();
	int				mw=w>>1;			//half width
	int				mh=h>>1;			//half height
	AmirrorFront	*front=(AmirrorFront *)this->front;
	Avideo			*out=((AmirrorBack *)back)->out;
	Avideo			*in=((AmirrorBack *)back)->in;
	out->enter(__FILE__,__LINE__);
	in->enter(__FILE__,__LINE__);
	{
		Abitmap	*bo=out->getBitmap();
		Abitmap	*bi=in->getBitmap();
		bool	reset=front->reset->get();
		if (reset)
		{
			front->reset->set(false);
			front->horz->set(false);
			front->vert->set(false);
			front->cross->set(false);
			front->upDiag->set(false);
			front->downDiag->set(false);
			front->diag->set(false);
			front->star->set(false);				
		}
		if(bo) // output pin connected
		{
			// Get UI values
			bool	horz=front->horz->get();
			bool	vert=front->vert->get();
			bool	cross=front->cross->get();
			if (horz && vert) {	cross = true; }

			bool	upDiag=front->upDiag->get();
			bool	downDiag=front->downDiag->get();
			bool	diag=front->diag->get();
			if (upDiag && downDiag) { diag = true; }
			
			bool	star = front->star->get();
			if (diag && cross) { star = true; }

			bool outside =  !front->inside->get(); // default value is false! so we have to not it (quick fix) - DOUBLE CHECK THIS !!!

			bool flipInX =  front->flipInX->get();
			bool flipInY =  front->flipInY->get();

			float	xx,yy;						//position input at front right of module.
			front->zone->get(&xx, &yy);
			int	x=(int)(xx*mw);
			int	y=(int)(yy*mh);

			if(bi)	//input pin connected
			{
				// TODO - add bypass switch on UI   -- LOOK AT FREEFRAME MODULE TO SEE HOW THIS IS DONE PROPERLY !!

		/*		bool bypass = true;
				if (bypass)
				{
					bo->set(0, 0, bi, bitmapNORMAL, bitmapNORMAL);
					goto endLabel; //need to set label
				}
		*/				

				//TESTING some onbeat stuff
/*

				if (beat >= 3.0f){	// it's (pretty much) never exactly 3, so >= works where == doesn't.) would modulus work?
					downDiag = true;
					upDiag = false;
				}
*/
//				double beatCount = (int)beat;

				if ( (beatCount < 5) && ((long long)beat % 5 == 1) ) //1 to 4 - have to convert to 'long long' (8 bytes) because converting to int causes loss of precision =/
				{
					beatCount++;
				}
				else
				{
					beatCount = 1;
				}

				if (beatCount == 1) { horz = true; }
				else if (beatCount == 2) { vert = true;	}
				else if (beatCount == 3) { cross = true; }
				//if (b % 1 == 0) {
				else { diag = true; }


				//THIS WORKED SO JUST DO THIS FFS GRR
/*
				if ( (int)beat == 2)
				{
					downDiag = true;
					upDiag = false;
				}

				if ( (int)beat == 4)
				{
					downDiag = false;
					upDiag = true;
				}
				*/

				//if (dbeat >= 0.5) { star = true; } //doesn't seem to do anything/still don't know what dbeat is. apparently _not_ downbeat.
				
				//end onbeat testing


				mirror->set(0, 0, bi, bitmapNORMAL, bitmapNORMAL); // maybe this should go back at the end of function now?
				
				if (flipInX || flipInY)
				{
					fset(mirror, 0, 0, w, h, bi, 0, 0, flipInX, flipInY);
				}

				//do mirrors
				if(star)		// this is basically diag + cross == 'star'. vert, horz, etc are ignored.
				{
					this->star(mirror, quart, x, y, mw, mh, outside);
				}
				else if (diag)	// basically upDiag + downDiag. Y position (up/down) is ignored. star, upDiag and downDiag are ignored.
				{ 
//						if (horz) { this->horz(mirror, bi, y, mh, w, outside, flipInX, flipInY); }// should go after the f/xdiags, otherwise doesnt give the expected results.
//						if (vert) { this->vert(mirror, bi, x, mw, h, outside, flipInX, flipInY); }
						
						// so possibly what should be done is:
						// set quart to appropriate half of the image (left or top)
						// send quart to fdig/xdiag
						// use fset to mirror.
						// this might not work because sending half the img to f/xdiag might produce incorrect result =/
						// in which case just use fset to mirror result of f/xdiag, and discard unused part ( muh performance =( )

						// or - just do fdiag twice? and then fset twice. might work.


						fdiag(mirror, !outside);	//pretty sure this could be optimised by doing horz/vert cases first, only rendering what's needed and then
						xdiag(mirror, !outside);	//only doing this if !horz and !vert. (otherwise wasting time/resources rendering stuff that mever gets seen)
						if (horz) 
						{
							//works well
							top->set(0, 0, 0, y, w, mh, mirror, bitmapNORMAL, bitmapNORMAL);
							this->horz(mirror, top, 0, mh, w, true, flipInX, flipInY);
						}

						else if (vert) 
						{
						//	fset(mirror, mw, 0, mw, h, mirror, 0, 0, true, false); 
							left->set(0, 0, x, 0, mw, h, mirror, bitmapNORMAL, bitmapNORMAL);
							this->vert(mirror, left, 0, mw, h, true, flipInX, flipInY);
						}
				}

				else if(cross)	//still seems to ignore flipInX/Y ...
				{
					quart->set(0, 0, x, y, mw, mh, mirror, bitmapNORMAL, bitmapNORMAL);
					if (horz && vert) { outside = !outside; }
//					if (flipInX) { horz = !horz; }
//					if (flipInY) { vert = !vert; } 
					this->cross(mirror, quart, x, y, mw, mh, vert, horz, outside); // stil borked!! need to use 'bi' instead of mirror.
					//actually need to use Quart instead of bi, otherwise flipInX/Y aren't applied...
						
				}

				else if (upDiag)				//still need to handle cross, horz, vert	- X and Y position can't be used (unless vert+horz added)
				{
					this->upDiag(mirror, cross, outside); // why use cross?? same/similar to star
				}

				else if (downDiag)			//still need to handle cross, horz, vert	- X and Y position can't be used (unless vert+horz added)
				{
					this->downDiag(mirror, cross, outside);
				}

				else if(vert)		
				{
					this->vert(mirror, bi, x, mw, h, outside, flipInX,flipInY);
				}

				else if(horz) // borked when used with 'vert' !!! (because of the way 'cross' is implemented.
				{
					this->horz(mirror, bi, y, mh, w, outside, flipInX,flipInY);
				}

				else
				{
//					mirror->set(0, 0, bi, bitmapNORMAL, bitmapNORMAL);	//disable this?
				}
				bo->set(0, 0, mirror, bitmapNORMAL, bitmapNORMAL); // final output
			}
		}
	}
endLabel:
	//add labels so can jump to here if bypass == true.
	in->leave();
	out->leave();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//mirror operations

void Amirror::vert(Abitmap *mirror, Abitmap *bi, int x, int mw, int h, bool outside, bool flipInX, bool flipInY)
{
	if ( outside ) //mirrored from edge
	{
		fset(mirror,  0,  0, mw, h, bi, x, 0, flipInX, flipInY); 
		fset(mirror, mw,  0, mw, h, bi, x, 0, !flipInX, flipInY);
	}
	else //normal
	{
		fset(mirror,  0,  0, mw, h, bi, x, 0, !flipInX, flipInY);
		fset(mirror, mw,  0, mw, h, bi, x, 0, flipInX, flipInY);
	}
}

void Amirror::horz(Abitmap *mirror, Abitmap *bi, int y, int mh, int w, bool outside, bool flipInX, bool flipInY)
{
	if ( outside )
	{
		fset(mirror,  0,  0, w, mh, bi, 0, y, flipInX, flipInY);
		fset(mirror,  0, mh, w, mh, bi, 0, y, flipInX, !flipInY);
	}
	else
	{
		fset(mirror,  0,  0, w, mh, bi, 0, y, flipInX, !flipInY);
		fset(mirror,  0, mh, w, mh, bi, 0, y, flipInX, flipInY);
	}
}

void Amirror::upDiag(Abitmap *mirror, bool cross, bool outside)
{
	if ( !outside )
	{
		cross = !cross;
	}
	mirror->flipY();		//flip input image, can't do this directly to 'bi' or the preview image also changes
	fdiag(mirror, cross);	//diag it (top left to bottom right)
	mirror->flipY();		//flip image back so its now diag bottom left to top right
}

void Amirror::downDiag(Abitmap *mirror, bool cross, bool outside) //use fset() to add horz/vert mirror combos?
{
	if ( !outside )
	{
		cross = !cross;
	}
	fdiag(mirror, cross);
}

void Amirror::cross(Abitmap *mirror, Abitmap *source, int x, int y, int mw, int mh, bool vert, bool horz, bool outside)
{
	if ( !outside ) 
	{
		vert = !vert;
		horz = !horz;
	}
	fset(mirror,  0,  0, mw, mh, source, 0, 0, vert, horz); //potentially not needed?
	fset(mirror, mw,  0, mw, mh, source, 0, 0, !vert, horz);
	fset(mirror,  0, mh, mw, mh, source, 0, 0, vert, !horz);
	fset(mirror, mw, mh, mw, mh, source, 0, 0, !vert, !horz);
}

void Amirror::star(Abitmap *mirror, Abitmap *quart, int x, int y, int mw, int mh, bool outside)
{
	quart->set(0, 0, x, y, mw, mh, mirror, bitmapNORMAL, bitmapNORMAL);
					//quater the image
/**					if ( !outside ){
						cross = !cross;
						vert = !vert;
						horz = !horz;
					}
**/
					//fdiag(quart, cross);											//mirror the quatered image diagonally
	fdiag(quart, !outside);											//mirror the quartered image diagonally
	fset(mirror,  0,  0, mw, mh, quart, 0, 0, false, false);			//mirror the quartered image to the other quaters
	fset(mirror, mw,  0, mw, mh, quart, 0, 0, true, false);
	fset(mirror,  0, mh, mw, mh, quart, 0, 0, false, true);
	fset(mirror, mw, mh, mw, mh, quart, 0, 0, true, true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Amirror::fset(Abitmap *bd, int xd, int yd, int w, int h, Abitmap *bs, int xs, int ys, bool flipX, bool flipY)
{
	int	dx=flipX?-1:1;		//set whether to fill from left or right
	int	dy=flipY?-1:1;		//set whether to fill up or down
	int	i,j;
	xs+=flipX?(w-1):0;		//start from right of source, or left
	ys+=flipY?(h-1):0;		//start from bottom or source, or top
	for(j=0; j<h; j++)		//for each column
	{
		dword	*s=&bs->body32[bs->adr[ys]+xs];	//get pixel at source 
		dword	*d=&bd->body32[bd->adr[yd]+xd]; //get destination pixel
		for(i=0; i<w; i++)	//for each row
		{
			*d=*s;			//put source pixel into destination
			s+=dx;			//increment source pixel address by 1 or -1 depending in direction of fill
			d++;			//increment destination pixel address by 1 (column)
		}
		ys+=dy;			//go up or down one row
		yd++;			//increment destination pixel address by 1 (row)
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//mirrors the image diagonally (90 degrees, with stretch)
//only manipulates the part of the image that changes, the rest is left untouched.
void Amirror::fdiag(Abitmap *bd, bool inv)
{
	int	w=bd->w;		//w = input bitmap width										//for sake of argument: 320
	int	h=bd->h;		//h = input bitmap hieght										//						240
	//pixels have an X and Y address ofc
	int	yd=(h<<16)/w;	//set destination pixel to height bitshifted by 16/width		//49152 - because each pixel is 16 bits? dividing by the width gives us a whole column?
	int	xd=(w<<16)/h;	//set destination pixel to width bitshifted by 16/height						87381.33333333333 - dividing by the height gives us a whole row?
	int	x=0;			//starting at top right of img
	int y=0;
	if(inv)
	{
		for(x=0; x<w; x++)			//for each column
		{
			int		y0;				//y (height location) of current pixel
			int		x0=x<<16;		//x (width location) of current pixel = current x pos bitshifted left by 16
			dword	*s=&bd->body32[bd->adr[y>>16]];		//set source pixel to initial y bitshifted right by 16 
			dword	*d=&bd->body32[bd->adr[y>>16]+x];	//set dest pixel to inital y bitshifted right by 16, plus initial x

			for(y0=(y>>16); y0<h; y0++)	//for each row
			{
				*d=s[x0>>16];				//set value of destination pixel equal to source pixel in current column
				d+=w;						//increment destination address by image width
				x0+=xd;						//add to x (width) location of current pixel the value of xd (dest pixel bitshifted right, divided by height)
			}

			y+=yd;					//add to y (height) location addr of current pixel the value of yd (dest pixel height addr bitshifted by 16, divided by width)
		}
	}
	else
	{
		for(x=0; x<w; x++)			//for every pixel in the current row, which has width w (loop through the columns??)
		{
			int		y0=y;			//current destination pixel y addr
			int		x0;				//current destination pixel x addr

			dword	*s=&bd->body32[x];					//set pixel at source location x?
			dword	*d=&bd->body32[bd->adr[y0>>16]+x];	//set destination pixel to be current height y + width x (first itr: 0 + 0, 2nd itr: 

			for(x0=x; x0<w; x0++)						//for every pixel in the current row
			{
				*(d++)=s[(y0>>16)*w];						//set the value of destination pixel to pixel at source current height * width, then increment destination addr?
				y0+=yd;										//new height = current height + yd ??
			}

			y+=yd;										//new height = current height + yd ??, then do next row?
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//mirrors the image diagonally (180 degrees)
void Amirror::xdiag(Abitmap *bd, bool inv)
{
	int	w=bd->w;		//w = input bitmap width										//for sake of argument: 320
	int	h=bd->h;		//h = input bitmap hieght										//						240
	//pixels have an X and Y address ofc
	int	yd=(h<<16)/w;	//set destination pixel to height bitshifted by 16/width		//49152 - because each pixel is 16 bits? dividing by the width gives us a whole column?
	int	xd=(w<<16)/h;	//set destination pixel to width bitshifted by 16/height						87381.33333333333 - dividing by the height gives us a whole row?
	int	x=0;			//starting at top right of img
	int y=0;

//	Abitmap *bs = bd;
//	bs = new Abitmap(bd); //nope, memory leak.
	bs = bd;	//causes crash? IF NOT SET TO NULL BEFORE METHOD RETRUNS

//	bs->set(0, 0, bd, bitmapNORMAL, bitmapNORMAL); //fixes crash, breaks effect =( well it works, but needs more flipping etc - This is probably a better way of doing things tho
	
	fdiag(bd, inv);

	bs->flipX();

	{
		for(x=0; x<w; x++)			//for each column
		{
			int		y0;				//y (height location) of current pixel
			int		x0=x<<16;		//x (width location) of current pixel = current x pos bitshifted left by 16
			dword	*s=&bs->body32[bs->adr[y>>16]];		//set source pixel to initial y bitshifted right by 16		--		SOURCE IS NOW COPY OF THE ORIGINAL!
			dword	*d=&bd->body32[bd->adr[y>>16]+x];	//set dest pixel to inital y bitshifted right by 16, plus initial x

			for(y0=(y>>16); y0<h; y0++)	//for each row
			{
				*d=s[x0>>16];				//set value of destination pixel equal to source pixel in current column
				d+=w;						//increment destination address by image width
				x0+=xd;						//add to x (width) location of current pixel the value of xd (dest pixel bitshifted right, divided by height)
			}

			y+=yd;					//add to y (height) location addr of current pixel the value of yd (dest pixel height addr bitshifted by 16, divided by width)
		}
		bd->flipX();
	}
	bs = NULL;		//sort of seems to fix crash BUT MEMORY USAGE IS PRETTY BLOODY HIGH!
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AmirrorFront::paint(Abitmap *b)
{
	b->set(0, 0, back, bitmapDEFAULT, bitmapDEFAULT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AmirrorFront::AmirrorFront(QIID qiid, char *name, Amirror *e, int h) : AeffectFront(qiid, name, e, h)
{
	Aresobj	o=resdll.get(MAKEINTRESOURCE(IDB_PNG7), "PNG");
	back=new Abitmap(&o);

	//all need new postions (except 'inside' and 'position')
	inside=new ActrlButton(MKQIID(qiid, 0xdc4809ccccef97c1), "Type", this, 27, 18, 14, 27, &resdll.get(MAKEINTRESOURCE(IDB_PNG8), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	inside->setTooltips("Mirror type");
	inside->show(TRUE);

	flipInX=new ActrlButton(MKQIID(qiid, 0xdc4809ccccef97c2), "Flip X", this, 61, 18, 14, 27, &resdll.get(MAKEINTRESOURCE(IDB_PNG8), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	flipInX->setTooltips("Flip input left/right");
	flipInX->show(TRUE);

	flipInY=new ActrlButton(MKQIID(qiid, 0xdc4809ccccef97c3), "Flip Y", this, 83, 18, 14, 27, &resdll.get(MAKEINTRESOURCE(IDB_PNG8), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	flipInY->setTooltips("Flip input up/down");
	flipInY->show(TRUE);

	vert=new ActrlButton(MKQIID(qiid, 0xdc4809aabbef97c0), "Mirror/Vertical", this, 108, 6, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG5), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	vert->setTooltips("Vertical mirror");
	vert->show(TRUE);

	horz=new ActrlButton(MKQIID(qiid, 0xc000267a5dc94030), "Mirror/Horizontal", this, 195, 6, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG3), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	horz->setTooltips("Horizontal mirror");
	horz->show(TRUE);

	cross=new ActrlButton(MKQIID(qiid, 0x4fcca907461f88bc), "Mirror/Cross", this, 282, 6, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG1), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	cross->setTooltips("Cross mirror");
	cross->show(TRUE);

	upDiag=new ActrlButton(MKQIID(qiid, 0x67bdf9e422c6a600), "Mirror/Up Diagonal", this, 108, 34, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG11), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	upDiag->setTooltips("Upwards diagonal mirror");
	upDiag->show(TRUE);

	downDiag=new ActrlButton(MKQIID(qiid, 0x67bdf9e422c6a601), "Mirror/Down Diagonal", this, 195, 34, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG9), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	downDiag->setTooltips("Downwards diagonal mirror");
	downDiag->show(TRUE);

	diag=new ActrlButton(MKQIID(qiid, 0x67bdf9e422c6a602), "Mirror/Diagonal", this, 282, 34, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG2), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	diag->setTooltips("Diagonal mirror");
	diag->show(TRUE);

	star=new ActrlButton(MKQIID(qiid, 0x67bdf9e422c6a603), "Mirror/Star", this, 369, 6, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG10), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	star->setTooltips("Star mirror");
	star->show(TRUE);

	reset=new ActrlButton(MKQIID(qiid, 0x27bdf9e422c6a601), "Mirror/Reset", this, 369, 34, 71, 24, &resdll.get(MAKEINTRESOURCE(IDB_PNG4), "PNG"), Abutton::bt2STATES|Abutton::btBITMAP);
	reset->setTooltips("Reset mirrors");
	reset->show(TRUE);

	zone=new Azone(MKQIID(qiid, 0x032d01127efc8200), "Source position", this, 463, 10, 32, 32, 0.5f, 0.5f);
	zone->setTooltips("Source position");
	zone->show(TRUE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AmirrorFront::~AmirrorFront()
{
	delete(inside);
	delete(back);
	delete(vert);
	delete(horz);
	delete(cross);
	delete(diag);
	delete(zone);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool AmirrorFront::notify(Anode *o, int event, dword p)
{
	switch(event)
	{
	case nyCHANGE:
		break;

	case nyCLICK:
		break;
	}
	return AeffectFront::notify(o, event, p);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//not needed?
void AmirrorFront::pulse()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AmirrorBack::AmirrorBack(QIID qiid, char *name, Amirror *e, int h) : AeffectBack(qiid, name, e, h)
{
	Aresobj	o=resdll.get(MAKEINTRESOURCE(IDB_PNG6), "PNG");
	back=new Abitmap(&o);

	out=new Avideo(MKQIID(qiid, 0xabf678edc8d7ce3f),  "video output", this, pinOUT, pos.w-18, 10);
	out->setTooltips("video output");
	out->show(TRUE);

	in=new Avideo(MKQIID(qiid, 0x9bb8a73deb9621e4), "video input", this, pinIN, 10, 10); //change qiidsph[i] to new random GUID!!
	in->setTooltips("video input");
	in->show(TRUE);

	new Aitem("Visit website", "Visit website", context, contextCLEAR, this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool AmirrorBack::notify(Anode *o, int event, dword p){

	if (event == nyCONTEXT)
	{
		Aitem *i=(Aitem *)p;
		if(i->data == contextCLEAR){
			ShellExecute(getWindow()->hw, "open", "https://alphatangovideo.wordpress.com/", NULL, NULL, SW_SHOWNORMAL);
			return true;
		}
	}
	return AeffectBack::notify(o, event, p);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AmirrorBack::~AmirrorBack()
{
	delete(out);
	delete(back);
	delete(in);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AmirrorBack::paint(Abitmap *b)
{
	b->set(0, 0, back, bitmapDEFAULT, bitmapDEFAULT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AmirrorInfo::~AmirrorInfo()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Aeffect * AmirrorInfo::create(QIID qiid, char *name, Acapsule *capsule)
{
	return new Amirror(qiid, name, this, capsule);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" 
{
	DLLEXPORT class Aplugz * getPlugz()
	{
		return new AmirrorInfo("mirror", &Amirror::CI, "mirror", "mirror");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
